private class ParlezVousTask extends AsyncTask<String, Void, Boolean> {
 
	@Override
	protected void onPreExecute() {
		// exécuté dans l'UIThread
	}
 
	@Override
	protected Boolean doInBackground(String... params) {
 
		// exécuté sur un autre thread
 
		return true;
	}
 
	@Override
	protected void onPostExecute(Boolean result) {
		// exécuté dans l'UIThread
	}
}