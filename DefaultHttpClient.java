try {
	DefaultHttpClient client = new DefaultHttpClient();
	HttpGet request = new HttpGet("http://www.google.com");
	HttpResponse response = client.execute(request);
 
	InputStream content = response.getEntity().getContent();
 
	// ...
 
} catch (ClientProtocolException e) {
	e.printStackTrace();
} catch (IOException e) {
	e.printStackTrace();
}