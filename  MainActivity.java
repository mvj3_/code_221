public class MainActivity extends Activity {
 
	private TextView myTextView;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
 
		myTextView = (TextView) findViewById(R.id.my_text_view);
 
		myTextView.setText(R.string.hello_world);
	}
}